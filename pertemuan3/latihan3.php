<?php
/*
PENGONDISIAN / PERCABANGAN
    1. if else
    2. if else if else
    3. ternary
    4. switch
*/

// if
    $x = 70;
    if ($x < 20) {
        echo "benar";
    } else if ($x == 20) {
        echo "Selamat!";
    } else {
        echo "salah";
    }
    echo "<br>";

// switch
    $y = 0;
    switch($y) {
        case 10 :
            echo "anda memasukkan angka 10";
            break;
        case 20 :
            echo "anda memasukkan angka 20";
            break;
        case 30 :
            echo "anda memasukkan angka 30";
            break;
        default :
            echo "anda memasukkan angka yang salah";
            break; // break berfungsi untuk mengakhiri switch
    }
    echo "<br>";

    $makanan = "pizza";
    switch($makanan) {
        case "nasi" :
        case "sate" :
        case "daging" :
            echo "Wahh, sehat!";
            break;  // bisa dibuat seperti ini untuk case dengan output yang sama
        case "hamburger" :
        case "mi" :
        case "fastfood" :
            echo "Jangan, itu gak sehat!";
            break;
        default :
            echo "anda memasukkan nama makanan yang salah!";
            break;
    }
?>