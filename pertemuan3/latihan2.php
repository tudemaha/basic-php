<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan 2</title>
    <style>
        .warna-cell {
            background-color: silver;
        }
    </style>
</head>
<body>
    <!-- Versi Biasa -->
    <table border="1" cellpadding="10" cellspacing="0">
        <?php 
            for($i = 1; $i <= 3; $i++) {
                echo "<tr>";
                    for($j = 1; $j <= 5; $j++) {
                        echo "<td>$i,$j</td>";
                    }
                echo "<tr>";
            }
        ?>
    </table>
    <br>

    <!-- Versi Templating -->
    <table border="1" cellpadding="10" cellspacing="0">
        <?php for($x = 1; $x <= 3; $x++) : ?>   <!-- "{"" bisa diganti dengan : -->
            <tr>
                <?php for($y = 1; $y <= 5; $y++) : ?>
                    <td><?= "$x, $y"; ?></td> <!-- "php echo" bisa dignti dengan = -->
                <?php endfor ?>
            </tr>
        <?php endfor ?> <!-- "}" bisa diganti dengan endfor [endif, endforeach (sesuai kondisi)] -->
    </table>
    <br>

    <!-- Mengubah Warna Baris dan Kolom dengan if else -->
    <table border="1" cellpadding="10" cellspacing="0">
        <?php for ($i = 1; $i <= 10; $i++) : ?>
            <?php if($i % 2 == 0) : ?>
                <tr class="warna-cell"> <!-- Ubah warna baris -->
            <?php else : ?>
                <tr>
            <?php endif ?>
                    <?php for($j = 1; $j <= 10; $j++) : ?>
                        <?php if($j % 2 == 0) : ?> <!-- Ubah warna kolom -->
                            <td class="warna-cell">
                        <?php else : ?>
                            <td>
                        <?php endif ?>
                            <?= "$i, $j"; ?>
                            </td>
                    <?php endfor ?>
                </tr>
        <?php endfor ?>
    </table>

</body>
</html>