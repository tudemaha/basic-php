<?php
/*
PENGULANGAN
1. for
2. while
3. do..while
4. foreach (spesifik untuk array)
*/

//for
    for($i = 0; $i<5; $i++) {
        echo "Hello World!<br>";
}
    echo "<br>";

//while (selama kondisi bernilai true, lakukan hal yang akan dieksekusi)
    $x = 0;
    while($x < 5) {
        echo "Aku belajar PHP <br>";
        $x++;
    }
    echo "<br>";

//do while
    $a = 0;
    do {
        echo "Belajar PHP kuy!!<br>";
        $a++;
    } while ($a < 5);
?>