<?php
    require 'functions.php';

    // Cek apa submit sudah ditekan
    if(isset($_POST["submit"])) {

        // Cek keberhasilan insert
        if(insert($_POST) > 0) {
            echo "
                <script>
                    alert('Data berhasil ditambahkan!');
                    document.location.href = 'index.php';
                </script>
            ";
        } else {
            echo "
                <script>
                    alert('Data gagal diambahkan!');
                </script>
            ";
        }

    }

?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Tambah Data HP</title>
  </head>
  <body>
    <h1 class="text-center fixed-top bg-success text-white pb-2" >Tambah Data HP</h1>
    <div class="container mt-5 pt-4 col-md-4"> 
        <form action="" method="post">
            <div class="mb-3">
                <label for="brand" class="form-label">Brand</label>
                <input type="text" name="brand" id="brand" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="model" class="form=label">Model</label>
                <input type="text" name="model" id="brand" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="chipset" class="form-label">Chipset</label>
                <input type="text" name="chipset" id="chipset" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="storage" class="form-label">Storage</label>
                <input type="text" name="storage" id="storage" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="layar" class="form-label">Display</label>
                <input type="text" name="layar" id="layar" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="foto" class="form-label">Foto</label>
                <input type="text" name="foto" id="foto" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="link" class="form-label">GSM Arena Link</label>
                <input type="url" name="link" id="link" class="form-control" required>
            </div>
            <button type="submit" name="submit" class="btn btn-success">Tambah</button>
        </form>
    </div>
    <br>
    <div class="text-center mb-3">
        <a href="index.php" class="btn btn-primary">Kembali ke halaman sebelumnya</a>
    </div>
    <!-- Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </body>
</html>