<?php

function salam($waktu = "Datang", $nama = "Admin") {
    return "Selamat $waktu, $nama!";    // return untuk mengembalikan nilai

    // Hanya bekerja jika semua parameter telah diisi
    // Untuk mengatasi, isikan parameter default
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan Function</title>
</head>
<body>
    <h1><?= salam("Siang", "Mahardika"); ?></h1>
</body>
</html>