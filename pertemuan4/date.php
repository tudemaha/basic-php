<?php
    /*
    DATE / TIME
        1. time()
        2. date()
        3. mktime()
        4. strtotime()

    STRING
        1. strlen() [menghitung panjang string]
        2. strcmp() [membandingkan dua string]
        3. explode() [memecah string menjadi array]
        4. htmlspecialchars [mencegah terobosan ke website]

    UTILITY
        1. var_dump() [mengecek isi variable]
        2. isset() [mengecek apa variabel sudah pernah dipakai atau belum]
        3. empty() [mengecek variable kosong]
        4. die() [memberhentikan program]
        5. sleep() [memberhentikan sementara program]
    */

    // date
    date_default_timezone_set('Asia/Makassar');

    echo date("l, d F Y");  // Memerlukan setidaknya satu parameter
    echo "<br>";
    echo date("d F Y");
    echo "<br>";

    // time
    // UNIX Timestamp (EPOCH time)
    echo time();    // Menghitung detik dari 1 Januari 1970 sampai saat ini
    echo "<br>";
    
    echo date("l", time()+172800); // Tampilkan hari saat ini ditambah 2 hari yang akan datang (172800 = detik untuk 2 hari)
    echo "<br>";
    echo date("l, d M Y", time()-60*60*24*100);    // 100 hari sebelum sekarang
    echo "<br>";


    // mktime
    // membuat detik dari 1 Januari 1970
    // mktime(hour, minute, second, month, day, year)
    echo date("l", mktime(0, 0, 0, 8, 12, 2003));   // Mengetahi hari dari tanggal tertentu
    echo "<br>";
    echo time()-mktime(0, 0, 0, 8, 12, 2003);   //Mengetahui berapa detik dari waktu tertentu sampai saat ini
    echo "<br>";

    // strtotime
    // mengembalikan detik dari 1 Januari 1970 sampai string tanggal yang dimasukkan
    echo strtotime("12 aug 2003");
?>