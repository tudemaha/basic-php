<?php
// Cek apakah tidak ada data di $_GET
if(!isset($_GET["brand"]) ||
    !isset($_GET["model"]) ||
    !isset($_GET["chipset"]) ||
    !isset($_GET["storage"]) ||
    !isset($_GET["layar"]) ||
    !isset($_GET["foto"])) {
    // Redirect
    header("Location: latihan1.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detail Mahasiswa</title>
</head>
<body>
    <ul>
        <li><img src="imgtugas/<?= $_GET["foto"]; ?>" alt=""></li>
        <li><?= $_GET["brand"]; ?></li>
        <li><?= $_GET["model"]; ?></li>
        <li><?= $_GET["chipset"]; ?></li>
        <li><?= $_GET["storage"]; ?></li>
        <li><?= $_GET["layar"]; ?></li>
    </ul>

    <a href="latihan1.php">Kembali ke halaman sebelumnya</a>
</body>
</html>