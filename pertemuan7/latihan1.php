<?php
/*
SUPERGLOBALS (Semuanya sebagai array asosiative)
- $_GET => Mengirim data array melalui url atau form
- $_POST => Mengirim data array melalui form
- $_REQUEST
- $_SESSION
- $_COOKIE
- $_SERVER
- $_ENV
- $_FILES

Membuat variabel global:
    $x = 10;

    function coba() {
        global $x; => gunakan keyword global
        echo $x;
    }
*/

$handphones = [
    [
        "brand" => "Xiaomi",
        "model" => "Mi 11 Ultra",
        "chipset" => "Snapdragon 888 5G",
        "storage" => "12GB 256GB",
        "layar" => "6,81 inch",
        "foto" => "mi11.jpg"
    ],
    [
        "brand" => "Oppo",
        "model" => "Find X3 Pro",
        "chipset" => "Snapdragon 888 5G",
        "storage" => "16GB 512GB",
        "layar" => "6,7 inch",
        "foto" => "oppo-find-x3-pro.jpg"
    ],
    [
        "brand" => "Vivo",
        "model" => "X60 pro",
        "chipset" => "Snapdragon 870 5G",
        "storage" => "12GB 256GB",
        "layar" => "6,56 inch",
        "foto" => "x60-pro.jpg"
    ],
    [
        "brand" => "Huawei",
        "model" => "Mate 40 Pro",
        "chipset" => "Kirin 9000 5G",
        "storage" => "8GB 512GB",
        "layar" => "6,76 inch",
        "foto" => "mate40-pro.jpg"
    ],
    [
        "brand" => "Redmi",
        "model" => "K40 Gaming",
        "chipset" => "Dimensity 1200 5G",
        "storage" => "12GB 256GB",
        "layar" => "6,67 inch",
        "foto" => "k40-gaming.jpg"
    ],
    [
        "brand" => "Apple",
        "model" => "iPhone 12 Pro Max",
        "chipset" => "Apple A14 Bionic",
        "storage" => "6GB 512GB",
        "layar" => "6,7 inch",
        "foto" => "12-pro-max.jpg"
    ],
    [
        "brand" => "Asus",
        "model" => "Zenfone 8",
        "chipset" => "Snapdragon 888 5G",
        "storage" => "16GB 256GB",
        "layar" => "5,9 inch",
        "foto" => "zenfone-8.jpg"
    ],
    [
        "brand" => "Asus",
        "model" => "ROG Phone 5",
        "chipset" => "Snapdragon 888 5G",
        "storage" => "16GB 256GB",
        "layar" => "6,78 inch",
        "foto" => "rog-phone-5.jpg"
    ],
    [
        "brand" => "Xiaomi",
        "model" => "Mi 10 Pro 5G",
        "storage" => "12GB 256GB",
        "chipset" => "Snapdragon 865 5G",
        "layar" => "6,67 inch",
        "foto" => "mi-10-pro-5g.jpg"
    ],
    [
        "brand" => "Apple",
        "model" => "iPhone 11 Pro Max",
        "chipset" => "Apple A13 Bionic",
        "storage" => "4GB 512GB",
        "layar" => "6,5 inch",
        "foto" => "iphone-11-pro-max.jpg"
    ]
];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GET</title>
</head>
<body>
    <h1>Daftar HP Flagship</h1>
    <ul>
        <?php foreach($handphones as $handphone) : ?>
            <li>
                <a href="latihan2.php?foto=<?= $handphone["foto"]; ?>&brand=<?= $handphone["brand"] ;?>&model=<?= $handphone["model"]; ?>&chipset=<?= $handphone["chipset"]; ?>&storage=<?= $handphone["storage"]; ?>&layar=<?= $handphone["layar"]; ?>"><?= $handphone["brand"]." ".$handphone["model"]; ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
</body>
</html>