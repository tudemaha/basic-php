<?php
    session_start();

    if(isset($_SESSION["login"])) {
        header('Location: index.php');
        exit;
    }

    require 'functions.php';

    // cek ditekan tombol login
    if(isset($_POST["login"])) {

        $username = $_POST["username"];
        $password = $_POST["password"];

        $result = mysqli_query($conn, "SELECT * FROM users WHERE username = '$username'");
        
        // cek username
        if(mysqli_num_rows($result) === 1) {
            $userData = mysqli_fetch_assoc($result);
            // cek password
            if(password_verify($password, $userData["password"])) {
                // login session
                $_SESSION["login"] = true;

                header('Location: index.php');
                exit;
            }
        }
        $error = true;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Login</title>
</head>
<body>
    <h1>Halaman Login</h1>

    <?php if($error) : ?>
        <p style="color:red; font-style:italic">Username atau password salah!</p>
    <?php endif; ?>

    <form action="" method="post">
        <ul>
            <li>
                <label for="username">Username:</label>
                <input type="text" name="username" id="username" required>
            </li>
            <li>
                <label for="password">Password:</label>
                <input type="password" name="password" id="password">
            </li>
            <li>
                <button type="submit" name="login">Sign In</button>
            </li>
        </ul>
    </form>
</body>
</html>