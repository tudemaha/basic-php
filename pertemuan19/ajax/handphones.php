<?php
    require '../functions.php';

    $keyword = $_GET["keyword"];
    $handphones = search($keyword);
?>

<table class="table">
    <thead class="text-center">
        <th scope="col">No.</th>
        <th scope="col">Aksi</th>
        <th scope="col">Foto</th>
        <th scope="col">Brand</th>
        <th scope="col">Model</th>
        <th scope="col">Chipset</th>
        <th scope="col">Max Storage</th>
        <th scope="col">Display</th>
        <th scope="col">GSM Arena Link</th>
    </thead>
    <tbody class="text-center">
        <?php $i = 1;?>
        <?php foreach($handphones as $handphone) : ?>
            <tr>
                <th scope="row"><?= $i; ?></th>
                <td>
                    <a href="update.php?id=<?= $handphone["id"]; ?>" class="btn btn-primary mb-2">Ubah</a>
                    <a href="delete.php?id=<?= $handphone["id"]; ?>&foto=<?= $handphone["foto"] ?>" class="btn btn-primary mb-2" onclick="return confirm('Apakah Anda yakin untuk menghapus data HP?');">Hapus</a>
                </td>
                <td><img src="imgtugas/<?= $handphone["foto"]; ?>" alt="" width="100"></td>
                <td><?= $handphone["brand"]; ?></td>
                <td><?= $handphone["model"]; ?></td>
                <td><?= $handphone["chipset"]; ?></td>
                <td><?= $handphone["storage"]; ?></td>
                <td><?= $handphone["layar"]; ?></td>
                <td><a href="<?= $handphone["link"]; ?>" target="_blank">Go to link</a></td>
            </tr>
        <?php $i++; ?>
        <?php endforeach; ?>
    </tbody>
</table>