<?php
    // Database connection
    $conn = mysqli_connect("localhost", "root", "", "phpdasar");

    // Read query
    function query($query) {
        global $conn;
        $result = mysqli_query($conn, $query);
        $rows = [];
        while($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
        }
        return $rows;
    }


    // Insert query
    function create($data) {
        global $conn;
        $brand = htmlspecialchars($data["brand"]);
        $model = htmlspecialchars($data["model"]);
        $chipset = htmlspecialchars($data["chipset"]);
        $storage = htmlspecialchars($data["storage"]);
        $layar = htmlspecialchars($data["layar"]);
        $foto = htmlspecialchars($data["foto"]);
        $link = htmlspecialchars($data["link"]);

        // Insert data
        $query = "INSERT INTO handphones VALUES (
            '', '$brand', '$model', '$chipset', '$storage', '$layar', '$foto', '$link'
        )";

        mysqli_query($conn, $query);
        return mysqli_affected_rows($conn);
    }

    // Delete query
    function delete($id) {
        global $conn;
        mysqli_query($conn, "DELETE FROM handphones WHERE id = $id");
        return mysqli_affected_rows($conn);
    }

    // Update function
    function update($data) {
        global $conn;
        $id = $data["id"];
        $brand = htmlspecialchars($data["brand"]);
        $model = htmlspecialchars($data["model"]);
        $chipset = htmlspecialchars($data["chipset"]);
        $storage = htmlspecialchars($data["storage"]);
        $layar = htmlspecialchars($data["layar"]);
        $foto = htmlspecialchars($data["foto"]);
        $link = htmlspecialchars($data["link"]);

        //Update data
        $query = "UPDATE handphones SET
                brand = '$brand',
                model = '$model',
                chipset = '$chipset',
                storage = '$storage',
                layar = '$layar',
                foto = '$foto',
                link = '$link'
            WHERE id = $id;
        ";
        mysqli_query($conn, $query);
        return mysqli_affected_rows($conn);
    }

    // Cari data
    function search($keyword) {
        $query = "SELECT * FROM handphones WHERE
                    brand LIKE '%$keyword%' OR
                    model LIKE '%$keyword%' OR
                    chipset LIKE '%$keyword%' OR
                    storage LIKE '%$keyword%'
                ";
        return query($query);
    }
?>