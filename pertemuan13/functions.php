<?php
    // Database connection
    $conn = mysqli_connect("localhost", "root", "", "phpdasar");

    // Read function
    function query($query) {
        global $conn;
        $result = mysqli_query($conn, $query);
        $rows = [];
        while($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
        }
        return $rows;
    }


    // Insert function
    function create($data) {
        global $conn;
        $brand = htmlspecialchars($data["brand"]);
        $model = htmlspecialchars($data["model"]);
        $chipset = htmlspecialchars($data["chipset"]);
        $storage = htmlspecialchars($data["storage"]);
        $layar = htmlspecialchars($data["layar"]);
        $link = htmlspecialchars($data["link"]);

        // image upload
        $foto = upload();
        if(!$foto) {
            return false;
        }

        // Insert data
        $query = "INSERT INTO
            handphones (brand, model, chipset, storage, layar, foto, link)
            VALUES ('$brand', '$model', '$chipset', '$storage', '$layar', '$foto', '$link')";

        mysqli_query($conn, $query);
        return mysqli_affected_rows($conn);
    }

    // upload function
    function upload() {
        $fileName = $_FILES["foto"]["name"];
        $fileSize = $_FILES["foto"]["size"];
        $error = $_FILES["foto"]["error"];
        $tmpName = $_FILES["foto"]["tmp_name"];

        // cek apa ada gambar yang diuploaad
        if($error === 4) {
            echo "<script>
                    alert('Gambar belum diinputkan!');
                </script>";
            return false;
        }

        // cek tipe file yang diinputkan
        $validExtension = ["jpg", "jpeg", "png"];
        $imageExtention = strtolower(end(explode(".", $fileName)));
        if(!in_array($imageExtention, $validExtension)) {
            echo "<script>
                    alert('Masukkan file gambar (jpg, jpeg, atau png)!');
                </script>";
            return false;
        }

        // cek ukuran gambar
        if($fileSize > 1000000) {
            echo "<script>
                    alert('Gambar yang Anda masukkan lebih dari 1 MB!');
                </script>";
            return false;
        }

        // new image name
        $newFileName = uniqid().".".$imageExtention;

        // gambar valid
        move_uploaded_file("$tmpName", "imgtugas/".$newFileName);
        return $newFileName;
    }

    // Delete function
    function delete($id, $foto) {
        global $conn;
        mysqli_query($conn, "DELETE FROM handphones WHERE id = $id");
        unlink("imgtugas/".$foto);
        return mysqli_affected_rows($conn);
    }

    // Update function
    function update($data) {
        global $conn;
        $id = $data["id"];
        $brand = htmlspecialchars($data["brand"]);
        $model = htmlspecialchars($data["model"]);
        $chipset = htmlspecialchars($data["chipset"]);
        $storage = htmlspecialchars($data["storage"]);
        $layar = htmlspecialchars($data["layar"]);
        // $fotoLama = htmlspecialchars($data["fotoLama"]);
        $link = htmlspecialchars($data["link"]);

        // cek ada gambar baru
        if($_FILES["foto"]["error"] === 4) {
            $foto = $fotoLama;
        } else {
            $foto = upload();
            if(!$foto) {
                return false;
            }
        }
        

        //Update data
        $query = "UPDATE handphones SET
                brand = '$brand',
                model = '$model',
                chipset = '$chipset',
                storage = '$storage',
                layar = '$layar',
                foto = '$foto',
                link = '$link'
            WHERE id = $id;
        ";
        mysqli_query($conn, $query);
        return mysqli_affected_rows($conn);
    }

    // Cari data
    function search($keyword) {
        $query = "SELECT * FROM handphones WHERE
                    brand LIKE '%$keyword%' OR
                    model LIKE '%$keyword%' OR
                    chipset LIKE '%$keyword%' OR
                    storage LIKE '%$keyword%'
                ";
        return query($query);
    }
?>