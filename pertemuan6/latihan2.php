<?php
    // $students = [
    //     ["Mahardika", "23497526734", "Teknik Informatika", "gede@mahardika.com"],
    //     ["Tugus", "Teknik Listrik", "234975456", "tugus@listrik.com"]
    // ];

    // Assosiative array
    // key berupa string yang dibuat sendiri
    $students = [
        [
            "nama" => "Mahardika",
            "nim" => "345634592",
            "jurusan" => "Teknik Informatika",
            "email" => "mahardika@informatika.com",
            "foto" => "mahardika.jpg"
        ],
        [
            "email" => "tugus@mesin.com",
            "nama" => "Tugus",
            "nim" => "345073453",
            "jurusan" => "Teknik Mesin",
            "foto" => "siapa.jpg"
            // "tugas" => [87, 89, 90]
        ]
    ];
    // echo $students[1]["tugas"][2];  // array 3 dimensi

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Mahasiswa</title>
</head>
<body>
    <h1>Data Mahasiswa</h1>

    <?php foreach ($students as $student) : ?>
        <ul>
            <li>Foto: <img src="img/<?= $student["foto"]; ?>" alt=""></li>
            <li>Nama: <?= $student["nama"]; ?></li>
            <li>NIM: <?= $student["nim"]; ?></li>
            <li>Jurusan: <?= $student["jurusan"]; ?></li>
            <li>Email: <?= $student["email"]; ?></li>
        </ul>
    <?php endforeach; ?>

</body>
</html>