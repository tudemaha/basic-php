<?php
    require 'conn.php';

    $handphones = query("SELECT * FROM handphones");
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Daftar HP Flagship</title>
  </head>
  <body>
    <h1 class="text-center fixed-top bg-primary text-white" style="padding-bottom: 10px">Daftar HP Flagship</h1>

    <table class="table" style="margin-top: 4rem">
        <thead class="text-center">
            <th scope="col">No.</th>
            <th scope="col">Aksi</th>
            <th scope="col">Foto</th>
            <th scope="col">Brand</th>
            <th scope="col">Model</th>
            <th scope="col">Chipset</th>
            <th scope="col">Max Storage</th>
            <th scope="col">Display</th>
        </thead>
        <tbody class="text-center">
            <?php $i = 1;?>
            <?php foreach($handphones as $handphone) : ?>
                <tr>
                    <th scope="row"><?= $i; ?></th>
                    <td>
                        <a href="" class="btn btn-primary">Ubah</a>
                        <a href="" class="btn btn-primary">Hapus</a>
                    </td>
                    <td><img src="../imgtugas/<?= $handphone["foto"]; ?>" alt="" width="100"></td>
                    <td><?= $handphone["brand"]; ?></td>
                    <td><?= $handphone["model"]; ?></td>
                    <td><?= $handphone["chipset"]; ?></td>
                    <td><?= $handphone["storage"]; ?></td>
                    <td><?= $handphone["layar"]; ?></td>
                </tr>
            <?php $i++; ?>
            <?php endforeach; ?>
        </tbody>

    </table>


    <!-- Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </body>
</html>