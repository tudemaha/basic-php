<?php
    require 'conn.php';

    $handphones = query("SELECT * FROM handphones");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Handphone Flagship</title>
    <style>
        td {
            text-align: center;
        }
    </style>
</head>
<body>
    <h1>Daftar Handphone Flagship</h1>
    <table border="1" cellspacing="0" cellpadding="10">
        <tr>
            <th>No.</th>
            <th>Aksi</th>
            <th>Foto</th>
            <th>Brand</th>
            <th>Model</th>
            <th>Chipset</th>
            <th>Max Storage</th>
            <th>Layar</th>
        </tr>

        <?php $i = 1;?>
        <?php foreach($handphones as $handphone) : ?>
            <tr>       
                <td><?= $i; ?></td>
                <td><a href="">Ubah</a> | <a href="">Hapus</a></td>
                <td><img src="../imgtugas/<?= $handphone["foto"]; ?>" alt="" width="100"></td>
                <td><?= $handphone["brand"]; ?></td>
                <td><?= $handphone["model"]; ?></td>
                <td><?= $handphone["chipset"]; ?></td>
                <td><?= $handphone["storage"]; ?></td>
                <td><?= $handphone["layar"] ?></td>
            </tr>
            <?php $i++; ?>
            <?php endforeach; ?>
    </table>

</body>
</html>