<?php

require_once __DIR__ . '/vendor/autoload.php';

require 'functions.php';
$handphones = query("SELECT * FROM handphones");

$print = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Handphone</title>
    <link rel="stylesheet" href="css/print.css">
</head>
<body>
    <h1 align="center">Daftar HP Flagship</h1>
    <table border="1" cellpadding="10" cellspacing="0">
        <tr>
            <th>No</th>
            <th>Foto</th>
            <th>Brand</th>
            <th>Model</th>
            <th>Chipset</th>
            <th>Max Storage</th>
            <th>Display</th>
        </tr>';
        $i = 1;
        foreach($handphones as $handphone) {
            $print .= '<tr>
                <td>'. $i++ .'</td>
                <td><img src="imgtugas/'.$handphone["foto"].'" width="80"></td>
                <td>'.$handphone["brand"].'</td>
                <td>'.$handphone["model"].'</td>
                <td>'.$handphone["chipset"].'</td>
                <td>'.$handphone["storage"].'</td>
                <td>'.$handphone["layar"].'</td>
            </tr>';
        }

$print .= '</table>
</body>
</html>';

$mpdf = new \Mpdf\Mpdf();
$mpdf->WriteHTML($print);
$mpdf->Output('Daftar HP Flagship.pdf', "I");

?>