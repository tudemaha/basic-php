<?php
    session_start();

    if(!isset($_SESSION["login"])) {
        header('Location: login.php');
        exit;
    }

    require 'functions.php';

    $handphones = query("SELECT * FROM handphones");

    // Tombol cari diklik
    if(isset($_POST["search"])) {
        $handphones = search($_POST["keyword"]);
    }
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Daftar HP Flagship</title>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/script.js"></script>
    <style>
        .loading {
            width: 150px;
            position: absolute;
            top: 54px;
            left: 315px;
            z-index: -1;
            display: none;
        }

        @media print {
            .print {
                display: none;
            }
        }
    </style>
  </head>
  <body>
    <h1 class="text-center fixed-top bg-primary text-white pb-2">Daftar HP Flagship</h1>


    <section class="mt-5 pt-4 mb-3">
        <a href="logout.php" onclick="return confirm('Yakin untuk keluar?');" class="print">Logout</a> | <a href="print.php" class="print" target="_blank">Cetak</a>
        <form action="" method="post" class="print">
            <input type="text" name="keyword" size="30" autofocus placeholder="Masukkan keyword pencarian!" autocomplete="off" id="keyword">
            <button type="submit" name="search" id="tombol-cari">Cari</button>
            <img src="imgtugas/loading.gif" alt="loading-animation" class="loading">
        </form>

        <div id="container">
            <table class="table">
                <thead class="text-center">
                    <th scope="col">No.</th>
                    <th scope="col" class="print">Aksi</th>
                    <th scope="col">Foto</th>
                    <th scope="col">Brand</th>
                    <th scope="col">Model</th>
                    <th scope="col">Chipset</th>
                    <th scope="col">Max Storage</th>
                    <th scope="col">Display</th>
                    <th scope="col" class="print">GSM Arena Link</th>
                </thead>
                <tbody class="text-center">
                    <?php $i = 1;?>
                    <?php foreach($handphones as $handphone) : ?>
                        <tr>
                            <th scope="row"><?= $i; ?></th>
                            <td class="print">
                                <a href="update.php?id=<?= $handphone["id"]; ?>" class="btn btn-primary mb-2">Ubah</a>
                                <a href="delete.php?id=<?= $handphone["id"]; ?>&foto=<?= $handphone["foto"] ?>" class="btn btn-primary mb-2" onclick="return confirm('Apakah Anda yakin untuk menghapus data HP?');">Hapus</a>
                            </td>
                            <td><img src="imgtugas/<?= $handphone["foto"]; ?>" alt="" width="100"></td>
                            <td><?= $handphone["brand"]; ?></td>
                            <td><?= $handphone["model"]; ?></td>
                            <td><?= $handphone["chipset"]; ?></td>
                            <td><?= $handphone["storage"]; ?></td>
                            <td><?= $handphone["layar"]; ?></td>
                            <td class="print"><a href="<?= $handphone["link"]; ?>" target="_blank">Go to link</a></td>
                        </tr>
                    <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        
        <div class="text-center ms-3 print">
            <a href="create.php" class="btn btn-success">Tambah data HP</a>
        </div>
        
    </section>

    <!-- Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    
    
  </body>
</html>