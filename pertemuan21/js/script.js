$(document).ready(function () {
  // hilangkan tombol cari
  $("#tombol-cari").hide();

  // event ketika keyword diketik
  $("#keyword").on("keyup", function () {
    // munculkan icon loading
    $(".loading").show();

    // ajax menggunakan load
    // $("#container").load("ajax/handphones.php?keyword=" + $("#keyword").val());

    // $.get()
    $.get("ajax/handphones.php?keyword=" + $("#keyword").val(), function (data) {
      $("#container").html(data);
      $(".loading").hide();
    });
  });
});
