<?php
    require 'functions.php';

    $id = $_GET["id"];
    $foto = $_GET["foto"];

    if(delete($id, $foto) > 0) {
        echo "
            <script>
                alert('Data berhasil dihapus!');
                document.location.href = 'index.php';
            </script>
        ";
    } else {
        echo "
            <script>
                alert('Data gagal dihapus!');
                document.location.href = 'index.php';
            </script>
        ";
    }
?>