<?php
// Pertemuan 2 - Dasar PHP
/*
Sintaks PHP:
1. Standar Output
    a. echo, print => menampilkan output
        echo "Gede Mahardika";
        print "Gede Mahardika";

    b. print_r => menampilkan isi array (debugging)
        print_r ("Gede Mahardika");

    c. var_dump => melihat isi variabel (debugging)
        var_dump ("Gede Mahardika");

2. Penulisan Sintaks PHP
    a. PHP di dalam HTML
    b. HTML di dalam PHP (tidak disarankan)

3. Variabel dan Tipe Data
    - Variabel (tidak perlu mendefinisikan tipe data)
    - Tidak boleh diawali dengan angka, tapi boleh mengandung angka
    - Tidak boleh ada spasi
    - Tidak boleh menggunakan dash
4. Operator
    a. Aritmatika (+- * / %)
    b. Penggabung string/concatenate/concat (.)
    c. Assignment/penugasan (=, +=, -=, *=, /=, %=, .=)
    d. Perbandingan (<, >, <=, >=, ==, !=) => tidak mengecek tipe data
    e. Identitas (===, !==) => mengecek tipe data
    f. Logika (&&, ||, !)
*/

// Output
echo "Gede Mahardika </br>";

// Variabel
$nama = "Putra";

// Interpolasi (menggunakan petik dua)
echo "Halo, nama saya $nama </br>";
echo 'Halo, nama saya $nama </br>';

// Operator aritmatika
$x = 10;
$y = 20;
echo $x * $y;
echo "</br>";

// Opetator Concat
$nama_depan = "Gede";
$nama_belakang = "Mahardika";
echo $nama_depan . " " . $nama_belakang . "<br>";

// Operator Assignment
$a = 1;
$a -= 4;
echo $a;
echo "</br>";

// Operator Perbandingan
var_dump (1 != 5);
echo "</br>";

// Operator Identitas
var_dump (1 === "1");
echo "</br>";

// Operator Logika
var_dump (30 < 20 || ($x % 2 == 0));
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Belajar PHP</title>
</head>
<body>
    <!-- PHP di dalam HTML (Recommended) -->
    <h1>Selamat datang, <?php echo $nama; ?> </h1>

    <!-- HTML di dalam PHP -->
    <?php
        echo "<h1>Selamat datang, Mahardika</h1>";
    ?>
</body>
</html>