-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2021 at 03:58 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpdasar`
--

-- --------------------------------------------------------

--
-- Table structure for table `handphones`
--

CREATE TABLE `handphones` (
  `id` int(11) NOT NULL,
  `brand` varchar(10) DEFAULT NULL,
  `model` varchar(20) DEFAULT NULL,
  `chipset` varchar(25) DEFAULT NULL,
  `storage` varchar(10) DEFAULT NULL,
  `layar` varchar(10) DEFAULT NULL,
  `foto` varchar(30) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `handphones`
--

INSERT INTO `handphones` (`id`, `brand`, `model`, `chipset`, `storage`, `layar`, `foto`, `link`) VALUES
(1, 'Xiaomi', 'Mi 11 Ultra', 'Snapdragon 888 5G', '12GB 256GB', '6,81 inch', 'mi11.jpg', 'https://www.gsmarena.com/xiaomi_mi_11_ultra-10737.php'),
(2, 'Oppo', 'Find X3 Pro', 'Snapdragon 888 5G', '16GB 512GB', '6,7 inch', 'oppo-find-x3-pro.jpg', 'https://www.gsmarena.com/oppo_find_x3_pro-10627.php'),
(3, 'Vivo', 'X60 Pro', 'Snapdragon 870 5G', '12GB 256GB', '6,56 inch', 'x60-pro.jpg', 'https://www.gsmarena.com/vivo_x60_pro-10797.php'),
(4, 'Huawei', 'Mate 40 Pro', 'Kirin 9000 5G', '8GB 512GB', '6,76 inch', 'mate40-pro.jpg', 'https://www.gsmarena.com/huawei_mate_40_pro-10528.php'),
(5, 'Redmi', 'K40 Gaming', 'Dimensity 1200 5G', '12GB 256GB', '6,67 inch', 'k40-gaming.jpg', 'https://www.gsmarena.com/xiaomi_redmi_k40_gaming-10880.php'),
(6, 'Apple', 'iPhone 12 Pro Max', 'Apple A14 Bionic', '6GB 512GB', '6,7 inch', '12-pro-max.jpg', 'https://www.gsmarena.com/apple_iphone_12_pro_max-10237.php'),
(7, 'Asus', 'Zenfone 8', 'Snapdragon 888 5G', '16GB 256GB', '5,9 inch', 'zenfone-8.jpg', 'https://www.gsmarena.com/asus_zenfone_8-10893.php'),
(8, 'Asus', 'ROG Phone 5', 'Snapdragon 888 5G', '16GB 256GB', '6,78 inch', 'rog-phone-5.jpg', 'https://www.gsmarena.com/asus_rog_phone_5-10715.php'),
(9, 'Xiaomi', 'Mi 10 Pro 5G', 'Snapdragon 865 5G', '12GB 256GB', '6,67 inch', 'mi-10-pro-5g.jpg', 'https://www.gsmarena.com/xiaomi_mi_10_pro_5g-10055.php'),
(10, 'Apple', 'iPhone 11 Pro Max', 'Apple A13 Bionic', '4GB 512GB', '6,5 inch', 'iphone-11-pro-max.jpg', 'https://www.gsmarena.com/apple_iphone_11_pro_max-9846.php'),
(24, 'Asus', 'Smartphone for Snapd', 'Snapdragon 888 5G', '16GB 512GB', '6,78 inch', 'snapdragon-insiders.jpg', 'https://www.gsmarena.com/asus_smartphone_for_snapdragon_insiders-11006.php'),
(26, 'Samsung', 'Galaxy S21+ 5G', 'Exynos 2100', '8GB 256GB', '6,7 inch', 's21-plus.jpg', 'https://www.gsmarena.com/samsung_galaxy_s21+_5g-10625.php');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nim` char(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `jurusan` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `nama`, `nim`, `email`, `jurusan`, `gambar`) VALUES
(1, 'Gede Mahardika', '7456347863', 'gede@teknik.com', 'Teknik Informatika', 'mahardika.jpg'),
(2, 'Agus', '3749237489', 'agus@teknik.com', 'Teknik Mesin', 'siapa.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `handphones`
--
ALTER TABLE `handphones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `handphones`
--
ALTER TABLE `handphones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
