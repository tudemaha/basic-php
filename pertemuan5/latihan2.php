<?php

$angka = [2, 5, 34, 7, 34, 0, 3, "aku"];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pengulangan Array</title>
    <style>
        .kotak {
            width: 50px;
            height: 50px;
            background-color: salmon;
            text-align: center;
            line-height: 50px;
            margin: 3px;
            float: left;
        }
        .clear { clear: both; }
    </style>
</head>
<body>
    <!-- Pengulangan cara pertama -->
    <?php for($i = 0; $i < count($angka); $i++) : ?>
    <!-- Gunakan count untuk menghitung banyak elemen array agar pengulangan berjalan sesuai berapa jumlah elemen pada array  -->
    <!-- Pakai < agar tidak terjadi error -->
        <div class="kotak"><?= $angka[$i]; ?></div>
    <?php endfor; ?>

    <div class="clear"></div>

    <!-- Pengulangan cara kedua -->
    <?php foreach($angka as $a) : ?>
        <div class="kotak"><?= $a; ?></div>
    <?php endforeach; ?>
</body>
</html>