<?php
/*
ARRAY = variabel yang memiliki banyak nilai
= pasangan key dan value (key and value pair)
key => index (mulai dari 0)

Cara lama:
$hari = array("Senin", "Selasa", "Rabu");

Cara baru:
$bulan = ["Januari", "Februari", "Maret"];
$arr1 = [123, "tulisan", false] => array di PHP bisa menampung tipe data berbeda
*/

    $hari = array("Senin", "Selasa", "Rabu");
    $bulan = ["Januari", "Februari", "Maret"];
    $arr1 = [123, "tulisan", false];

    // Menampilkan array => menampilkan salah satu elemen array
    echo $arr1[0];
    echo "<br>";

    // var_dump() => menampilkan isi array lengkap dengan tipe data
    var_dump($hari);
    echo "<br>";

    // print_r() => menampilkan array tanpa tipe data
    print_r($bulan);
    echo "<br>";
    echo "<br>";


    // Menambahkan 1 elemen baru pada array
    var_dump($hari);    // Menampilkan array sebelum ditambahkan
    $hari[] = "Kamis";  // Menambahkan array
    $hari[] = "Jumat";
    echo "<br>";
    var_dump($hari);    // Menampilkan array setelah ditambahkan

?>