<?php
// Array multidimensi
$students = [
    ["Mahardika", "1235634846", "Teknik Informatika", "gede@cinta.com"],
    ["Agus", "328945612", "Teknik Mesin", "agus@mesin.com"]
];

// Contoh di atas adalah array numerik (array yang indeksnya angka)

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Mahasiswa</title>
</head>
<body>
    <h1>Data Mahasiswa</h1>

    <h3>Cara 1</h3>
        <!-- Cara 1 -->
        <?php foreach($students as $student) : ?>
            <ul>
                <?php foreach($student as $s) : ?>
                    <li><?= $s; ?></li>
                <?php endforeach; ?>
            </ul>
        <?php endforeach; ?>

    <h3>Cara 2</h3>
        <!-- Cara 2 (Bisa diisi nama data) -->
        <?php foreach($students as $student) : ?>
            <ul>
                <li>Nama: <?= $student[0]; ?></li>
                <li>NIM: <?= $student[1]; ?></li>
                <li>Jurusan: <?= $student[2]; ?></li>
                <li>Email: <?= $student[3]; ?></li>
            </ul>
        <?php endforeach ?>

    
</body>
</html>